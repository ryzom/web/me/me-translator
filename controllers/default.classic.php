<?php
/**
* @package   me
* @subpackage translator
* @author    me.ryzom.com
* @copyright 2019 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


class defaultCtrl extends Ry\UserController {
	public $moduleName = 'translator';
	
	private function _get_class_name($obj)
	{
		$classname = get_class($obj);
		if ($pos = strrpos($classname, '\\')) return substr($classname, $pos + 1);
		return $pos;
	}

	public function index() {

		$src_lang = $this->param('src_lang');
		$dst_lang = $this->param('dst_lang');
		$command = $this->param('command');
		$value = $this->param('value');
		$id = $this->param('id');
		$record_id = $this->param('record_id');
		$type = $this->param('type', 'me');

		define('IMAGE_PATH', \jApp::wwwPath().'images/translations/');
		if (!is_dir(IMAGE_PATH))
			mkdir(IMAGE_PATH);

		$previous_dst_lang = isset($_SESSION['translator-dst-lang'])?$_SESSION['translator-dst-lang']:Ry\Common::getUserLang();
		if ($dst_lang === null)
			$dst_lang = isset($_SESSION['translator-dst-lang'])?$_SESSION['translator-dst-lang']:Ry\Common::getUserLang();
		if ($src_lang === null)
			$src_lang = isset($_SESSION['translator-src-lang'])?$_SESSION['translator-src-lang']:'wk';
		if ($src_lang == $dst_lang)
			$src_lang = 'wk';
		$_SESSION['translator-src-lang'] = $src_lang;
		$_SESSION['translator-dst-lang'] = $dst_lang;
				
		
		if ($this->param('ajax') == '1') {
			$rep = $this->getResponse('htmlfragment');
			if (true/*$id && $record_id && $value*/) {
				$translator = Tr\Projects::spawn($id);
				if ($command == 'translate') {
					$translator->setTranslations($dst_lang, $value);
				} else if ($command == 'upload-image') {
					if ($this->user->checkAccess('translator:upload')) {
						$uri = substr($value, strpos($value, ',')+1);
						$encodedData = str_replace(' ','+',$uri);
						file_put_contents(IMAGE_PATH.$id.'__'.$record_id.'.png', base64_decode($encodedData));
					}
				}
				
				$tpl = new jTpl();
				
				$src = $translator->getTranslations($src_lang);
				if ($dst_lang == 'wk')
					$wk_dst = $translator->getTranslations('wk');
				else
					$wk_dst = $translator->getTranslations($dst_lang.'_wk');

				$dst = $translator->getTranslations($dst_lang);
				
				$tpl->assign('record', ['infos' => [$record_id, @$src[$record_id], @$wk_dst[$record_id], @$dst[$record_id], is_file(IMAGE_PATH.$id.'__'.$record_id.'.png')], 'id' => $record_id]);
				$tpl->assign('type', $this->_get_class_name($translator));
				$tpl->assign('header', $translator->getHeader($dst_lang));
				$tpl->assign('id', $id);
				$tpl->assign('can_delete', $this->user->checkAccess('translator:delete'));
				$tpl->assign('src_lang', $src_lang);
				$tpl->assign('dst_lang', $dst_lang);
				$rep->addContent($tpl->fetch('tr_element'));
			} else
				$rep->addContent('');
			return $rep;
		}
		else
			$rep = $this->_getResponse('html');
		
		Ry\Common::includeJSandCSS($rep);

		$rep->bodyTpl = 'me~main';
		
		Ry\Common::setupMessage($rep);

		$user = $this->user;
		$rep->body->assign('username', $user->name);
		$infos = $user->getBrief();
		$rep->body->assign('user', $infos);
		
		if ($id) {
			$translator = Tr\Projects::spawn($id);
			$search = $this->param('search');
			$display = $this->param('display', isset($_SESSION['translator-display'])?$_SESSION['translator-display']:'all');
			$_SESSION['translator-display'] = $display;

			$params = $_GET;
			unset($params['command']);
			$params['id'] = $id;
			$params['src_lang'] = $src_lang;
			$params['dst_lang'] = $dst_lang;
			$translator->updateStats($dst_lang);
			
			$filter = array();
			switch ($command) {
				case 'add':
					if ($value)
						$translator->addSentence($value);
					$search = '';
					return $this->redirect(jUrl::getFull('translator~index', $params));
				break;
				
				case 'delete':
					if ($user->checkAccess('translator:delete'))
						$translator->delSentence($value);
					$search = '';
					return $this->redirect(jUrl::getFull('translator~index', $params));
				break;
				
				case 'revert':
					$translator->revertTranslations($dst_lang, $value);
				break;
				
				case 'translate':
					if ($previous_dst_lang == $dst_lang && $value) { // only translate if lang don't have changed 
						$translator->setTranslations($dst_lang, $value);
					}
					return $this->redirect(jUrl::getFull('translator~index', $params));
				break;

				case 'validate':
					$translator->validateTranslations($dst_lang, $value);
				break;

				case 'unvalidate':
					$translator->unvalidateTranslations($dst_lang, $value);
			}
			
			if ($display == 'single') {
				if ($search)
					$filter = [$search];
				$display = 'all';
			} else if ($search)
				$filter = array_keys($translator->searchTranslations($search, $src_lang, $dst_lang));
			
			if ($display == 'latest')
				$filter = $translator->getLatestAdded();
			$page = 0;
			$offset = intval($this->param('page'));
			$src = $translator->getTranslations($src_lang, 0, NULL, $display == 'latest'?$filter:[]);
			
			if ($dst_lang == 'wk')
				$wk_dst = $translator->getTranslations($dst_lang);
			else
				$wk_dst = $translator->getTranslations($dst_lang.'_wk');
			$dst = $translator->getTranslations($dst_lang);
			
			$list = [];
			$count = 0;
			foreach($src as $i => $value) {
				$in_filter = in_array($i, $filter);
				$wk_dst_i = @trim($wk_dst[$i]);
				$notrad = substr($src[$i], 0 , 3) == '[e]' || substr($src[$i], 0 , 8) == '[notrad]';
				if (!$search || $in_filter) {
					if (
						($display == 'all' ||
						($display == 'latest' && $in_filter) ||
						($display == 'missing' && $wk_dst_i == '' && !$notrad) ||
						($display == 'unvalidated' && (!isset($dst[$i]) || $dst[$i] == '' || $dst[$i] != @$wk_dst[$i]) && !$notrad)
					)) {
						$list[] = ['infos' => [$i, @$src[$i], @$wk_dst[$i], @$dst[$i], is_file(IMAGE_PATH.$id.'__'.$i.'.png')], 'id' => $i];
					}
					$count++;
				}
			}

			$display_icons = ['all' => 'fas fa-list-ul', 'missing' => 'fas fa-ellipsis-v', 'unvalidated' => 'fas fa-tasks', 'latest' => 'fas fa-asterisk'];
			
			$count = count($list);

			if ($offset >= $count)
				$offset = 0;
				
			$list = array_slice($list, $offset, 50);
		
			$zp = ['header' => $translator->getHeader($dst_lang), 'type' => $this->_get_class_name($translator), 'items' => $list, 'id' => $id, 'name' => $translator::$name, 'can_delete' => $user->checkAccess('translator:delete'), 'langs' => \Ry\Common::getLangs(), 'src_lang' => $src_lang,
				'dst_lang' => $dst_lang, 'display' => $display, 'display_icons' => $display_icons, 'editAction' => 'edit', 'search' => $search, 'recordCount' => $count,
				'page' => $offset, 'offsetParameterName' => 'page', 'send_image_url' => \jUrl::getFull('', ['command' => 'upload-image', 'ajax' => 1])];
			
			$rep->body->assignZone('MAIN', 'edit', $zp);
		} else {
			$alllangs = \Ry\Common::getLangs();

			$projects = Tr\Projects::getList();
			$project_files = [];
			foreach($projects as $p) 
				$project_files[$p->file] = $p;
			
			$db = \Ry\DB::spawn('translations', 'translator');
			$db_ark = \Ry\DB::spawn('ark', 'translator');
			$events = $db_ark->get('events', ['#OR' => ['on_live' => 1, 'on_app_trad' => 1]]);
			$ark_events = [];
			$update_projects = false;
			foreach($events as $event) {
				if (!isset($project_files[$event->id])) {
					$update_projects = true;
					$dir = [];
					$parent = $event->folder_id;
					while ($parent) {
						$folder = $db_ark->getSingle('folders', ['id' => $parent]);
						array_push($dir, $folder->name);
						$parent = $folder->parent_id;
					}
					$trads = unserialize($event->trads);
					$total = 0;
					$validated = array_combine($alllangs, [0, 0, 0, 0, 0, 0]);
					$translated = array_combine($alllangs, [0, 0, 0, 0, 0, 0]);
					if (!$trads) {
						continue;
					}
						
					foreach($trads as $id => $trad) {
						if ($id == 'REWARD_MAIL_MESSAGE') {
							unset($trads['REWARD_MAIL_MESSAGE']);
							$db_ark->update('events', ['trads' => serialize($trads)], ['id' => $event->id]);
							continue;
						}
						$total++;
						foreach($alllangs as $lang => $code) {
							if ($trad[$lang]) {
								$translated[$lang]++;

								$wk = $db->getSingle('ark_temp', ['id' => $id, 'event_id' => $event->id]);
								if ($wk) {
									foreach($alllangs as $lang => $code) {
										if (!$wk->$lang)
											$validated[$lang]++;
									}
								} else 
									$validated[$lang]++;
							}
						}
					}
					$stats = [];
					foreach($alllangs as $lang => $code)
						$stats[$lang] = $translated[$lang].'/'.$validated[$lang];


					$db->insert('projects', [
						'name' => $event->folder_id == '2335'?ucwords(str_replace('_', ' ', $event->name)):implode(' / ', $dir) . ' / ' . $event->name,
						'type' => 'Ark',
						'file' => $event->id,
						'icon' => $event->folder_id == '2335'?'fa-tablet-button':'fa-dice-d20',
						'total' => $total, 
					]+$stats);
				}
			}

			if ($update_projects)
				$projects = Tr\Projects::getList();
							
			$finished = [];
			$unfinished = [];
			
			foreach($projects as $p) {
				list($validated, $translated) = explode('/', $p->$dst_lang);
				$translated = min($translated, $p->total);
				$validated = min($validated,  $p->total);
				$p_infos =  ['type' => $p->type, 'icon' => $p->icon, 'name' => $p->type == 'Ark'?'<a href="https://app.ryzom.com/app_arcc/index.php?action=mTrads_Edit&display_all=1&event='.$p->file.'" target="_blank">'.$p->name.'</a>':'<span class="translation-type-'.$p->icon.'">'.$p->name.'</span>', 'total' => $p->total, 'translated' => $translated, 'validated' => $validated, 'id' => $p->id];
				if ($p->total == $validated && $validated == $translated) {
					if ($p->icon != 'fa-dice-d20')
						$finished[] = $p_infos;
				} else
					$unfinished[] = $p_infos;
			}

			
			$langs = [];
			$alllangs = \Ry\Common::getLangs();
			foreach($alllangs as $lang => $code) {
				if ($lang != 'wk')
					$langs[$lang] = ucfirst($lang);
			}

			$zp = ['search_word' => '', 'viewAction' => '', 'page' => 0, 'recordCount' => '', 'listPageSize' => '',
				'langs' => $langs, 'dst_lang' => $dst_lang, 'finished' => $finished, 'unfinished' => $unfinished, 'offsetParameterName' => 'page', 'search_list' => Null];
			$rep->body->assignZone('MAIN', 'list', $zp);
		}
		
		return $rep;
	}

	public function search() {
		$rep = $this->_getResponse('html');
		$search =  $this->param('search');
		Ry\Common::includeJSandCSS($rep);
		Ry\Common::setupMessage($rep);
		
		$rep->bodyTpl = 'me~main';
		
		if (isset($_SESSION['translator-dst-lang']))
			$dst_lang = $_SESSION['translator-dst-lang'];
		else
			$dst_lang = \Ry\Common::getUserLang();

		$projects = Tr\Projects::getList();
		$results = [];
		foreach($projects as $p) {
			$translator = Tr\Projects::spawn($p->id);
			$found = $translator->searchTranslations($this->param('search') ,'wk', $dst_lang);
			if ($found) {
				$p->found = [];
				foreach($found as $k => $v) 
					$p->found[$k] = preg_replace('/('.$search.')/i', '<span class="badge-secondary">${1}</span>', substr($v, 0, 90).(strlen($v)>90?'[...]':''));
				$p->total_founds = count($found);
				$results[$p->id] = $p;
			}
		}
		
		$langs = [];
		$alllangs = \Ry\Common::getLangs();
		foreach($alllangs as $lang => $code) {
			if ($lang != 'wk')
				$langs[$lang] = ucfirst($lang);
		}
		
		$zp = ['langs' => $langs, 'dst_lang' => $dst_lang, 'search_word' => $search,  'search_list' => $results];
		$rep->body->assignZone('MAIN', 'list', $zp);
		return $rep;
	}

}

