<?php
/**
* @package   me
* @subpackage translator
* @author    me.ryzom.com
* @copyright 2019 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class projectsCtrl extends Ry\UserController {

	protected $dao = 'translations__projects';

	protected $form = 'projects';

	protected $dbProfile = 'translations';

	public $pluginParams = [];


}
