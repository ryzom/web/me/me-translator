<?php
/**
* @package   me
* @subpackage translator
* @author    me.ryzom.com
* @copyright 2019 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


class translatorModuleConfigurator extends \Jelix\Installer\Module\Configurator {

    public function getDefaultParameters() {
        return array();
    }

    function configure(\Jelix\Installer\Module\API\ConfigurationHelpers $helpers) {

    }
}