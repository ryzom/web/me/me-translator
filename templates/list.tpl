<div id="translation-bar">
	{foreach $langs as $l => $lang }
	<a href="{ryurl ':index', ['dst_lang' => $l]}" class="{if $dst_lang == $l}selected{/if}">{$lang}</a>
	{/foreach}
	<div id="translation-search">
	<form action="/translator/default/search">
		<input type="text" name="search" value="{$search_word}"/>
		<input type="submit" value="Search"/>
	</form>
	</div>
</div>

<div class="big-frame">
	<table class="records-list" width="100%">
{if $search_list}
	<tbody>
		{foreach $search_list as $result}
		<tr class="bg-blue white">
			{if $result->type == 'Ark'}
			<td>{$result->name}</td><td><a href="https://app.ryzom.com/app_arcc/index.php?action=mTrads_Edit&display_all=1&event={$result->file}" target="_blank"><i class="fa-solid fa-up-right-from-square" title="Open in Ark"></i></a></td>
			{else}
			<td>{$result->name}</td><td></td>
			{/if}
		</tr>
		
		{foreach $result->found as $found => $text}
		<tr class="{cycle array('odd','even')}">
			{if $result->name == 'Phrases Base'}
				<td><a href="https://gitlab.com/ryzom/ryzom-data/-/blob/me-translations/translations/translated/{$result->file}{$dst_lang}.txt" target="_blank"><i class="fa-brands fa-square-gitlab"></i> GitLab</a></td><td>{$text}</td>
			{else}
				<td><a href="/translator?id={$result->id}&search={$found}&display=single&src_lang=wk" target="_blank">{$found}</a></td><td>{$text}</td>
			{/if}
		</tr>
		{/foreach}
		<tr class="badge-primary"><td colspan="5"></td></tr>
		{/foreach}
	</tbody>
{else}

	<thead>
		<tr>
			<th>Project</th><th>Type</th><th>Total</th><th>Not Translated</th><th>Not Validated</th><th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $unfinished as $record}
		<tr class="{cycle array('odd','even')}">
			<td align="center">{$record['name']}</td>
			<td align="center"><i class="fa-solid {$record['icon']}" title="{$record['type']}"></i></td>
			<td align="center"><span class="cyan">{$record['total']}</span></td>
			<td align="center"><span class="pink">{$record['total'] - $record['translated']}</span></td>
			<td align="center"><span class="pink">{$record['total'] - $record['validated']}</span></td>
			<td align="center">
				<a href="{ryurl $viewAction,array('id' => $record['id'], 'display' => 'missing', 'src_lang' => 'en', $offsetParameterName => $page)}" target="_blank">{@jelix~crud.link.edit.record@}</a>
			</td>
		</tr>
		{/foreach}

		<tr class="badge-primary"><td colspan="5"></td></tr>

		{foreach $finished as $record}
		<tr class="{cycle array('odd','even')}">
			<td align="center">{$record['name']}</td>
			<td align="center"><i class="fa-solid {$record['icon']}" title="{$record['type']}"></i></td>
			<td align="center"><span class="cyan">{$record['total']}</span></td>
			<td align="center"><span class="lightgreen">{$record['total'] - $record['translated']}</span></td>
			<td align="center"><span class="lightgreen">{$record['total'] - $record['validated']}</span></td>
			<td align="center">
				<a href="{ryurl $viewAction,array('id' => $record['id'], 'display' => 'missing', 'src_lang' => 'en', $offsetParameterName => $page)}" target="_blank">{@jelix~crud.link.edit.record@}</a>
			</td>
		</tr>
		{/foreach}
	</tbody>
{/if}
	</table>
	{if $recordCount > $listPageSize}
	<p class="record-pages-list">{@jelix~crud.title.pages@} : {pagelinks $listAction, array(),  $recordCount, $page, $listPageSize, $offsetParameterName }</p>
	{/if}
</div>