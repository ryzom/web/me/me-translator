
<div class="inline-block toolbar-button padding-5 effect-enlarge">
	<form id="translator-edit" action="{ryurl '', array('id' => $id, 'command' => 'add', 'src_lang' => $src_lang, 'dst_lang' => $dst_lang, $offsetParameterName=>$page)}" method="POST">
		<input type="text" name="value" /><a class="hoverboard" href="{ryurl ''}" onclick="document.getElementById('translator-edit').submit(); return false;" class="img-link" style="margin: 5px"><i class="fas fa-plus-circle"></i></a>
	</form>
</div>
<div class="inline-block toolbar-button padding-5 effect-enlarge">
	<form id="translator-search" action="{ryurl '', array('id' => $id, 'src_lang' => $src_lang, 'dst_lang' => $dst_lang, $offsetParameterName=>$page)}" method="GET">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="src_lang" value="{$src_lang}"/>
		<input type="hidden" name="dst_lang" value="{$dst_lang}"/>
		<input type="hidden" name="{$offsetParameterName}" value="{$page}"/>
		<input type="hidden" name="display" value="{$display}"/>
		<input type="text" name="search" value="{$search}" /><a class="hoverboard" href="{ryurl ''}" onclick="document.getElementById('translator-search').submit(); return false;" class="img-link" style="margin: 5px"><i class="fas fa-search"></i></a>
	</form>
</div>
<div class="inline-block float-right">
{$name}
</div>
<div class="inline-block">
	{foreach array('all', 'missing', 'unvalidated', 'latest') as $disp}
		<a href="{ryurl '', ['id'=>$id, 'search' => $search, 'src_lang' => $src_lang, 'dst_lang' => $dst_lang, 'display' => $disp, $offsetParameterName => 0]}" class="button {if $display == $disp}selected{/if}" title="{t 'change_to'.$disp}"><i class="{$display_icons[$disp]}"></i> {t $disp}</a>
	{/foreach}
</div>

<div class="big-frame translations">
{if $recordCount > 50}
	{pagelinks '', ['id' => $id, 'search' => $search],  $recordCount, $page, 50, $offsetParameterName, ['area-size' => 20]}
{/if}
<div id="post-message"></div>
<form id="translations" action="{ryurl '', array('id' => $id, 'command' => 'translate', 'search' => $search, 'src_lang' => $src_lang, 'dst_lang' => $dst_lang, $offsetParameterName=>$page)}" method="POST">

<table cellpadding="0" cellspacing="0" class="records-list" width="100%">
<thead>
<tr>
	<th>Name</th>
	<th>
		{foreach $langs as $lang => $code}
			<a class="button {if $lang == $src_lang}selected{/if}" href="{ryurl '', array('id' => $id, 'command' => 'translate', 'search' => $search, 'src_lang' => $lang, 'dst_lang' => $dst_lang, $offsetParameterName=>$page)}">{$lang}</a>
		{/foreach}
	</th>
	<th></th>
	<th>DeepLize</th>
	<th>
			{foreach $langs as $lang => $code}
				<a class="button {if $lang == $dst_lang}selected{/if}" href="{ryurl '', array('id' => $id, 'command' => 'translate', 'search' => $search, 'src_lang' => $src_lang, 'dst_lang' => $lang, $offsetParameterName=>$page)}">{$lang}</a>
			{/foreach}
	</th>
	<th>Enable on Atys</th>
	<th>Action</th>
</tr>
</thead>
<tbody> 
	<tr><td align="center" colspan="7" class="bg-red bold">Please CTRL+S to save a change !</td></tr>
{foreach $items as $record}
<tr><td height="5px"></td></tr>
<tr id="tr-{$record['infos'][0]}" class="{cycle array('odd','even')}">
	{include 'tr_element'}
</tr>
{/foreach}
<!-- <tr><td colspan="6" align="right"><a href="{ryurl ''}" onclick="document.getElementById('translations').submit(); return false;"><div id="translation-send" class="badge-secondary round padding-5"><i class="fas fa-paper-plane"></i>&nbsp;SEND</div></a></td></tr> -->
</tbody>
</table>
</form>
{if $recordCount > 50}
	{pagelinks '', ['id' => $id, 'search' => $search],  $recordCount, $page, 50, $offsetParameterName, ['area-size' => 5]}
{/if}

</div>

<script>
{if $type == 'Words'}
		sendTranslations = sendTranslationWordsOptions;
{else}
		sendTranslations = saveTranslations;
{/if}
		
{literal}
	document.addEventListener('keydown', function(e) {
		if (e.keyCode == 83 && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) {
			e.preventDefault();
			sendTranslations();
		}
	}, false);

	document.onpaste = function(event) {
		
		// Check  event.target
		var items = (event.clipboardData || event.originalEvent.clipboardData).items;
		console.log(JSON.stringify(items)); // will give you the mime types
		for (index in items) {
			var item = items[index];
			if (item.kind === 'file') {
				var blob = item.getAsFile();
				var reader = new FileReader();
				reader.onload = function(event){
					var data = new FormData();
					var id = document.activeElement.id.substr(4);
{/literal}
					var url = '{$send_image_url}';
					data.append('id', '{$id}');
{literal}
					data.append('value', event.target.result);
					data.append('record_id', id);
					postAjax(url, 'tr-'+id, data, false);
				};
				reader.readAsDataURL(blob);
			}
		}
	}

{/literal}
</script>