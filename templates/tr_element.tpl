<td align="center" width="100px"><span class="small">{$record['infos'][0]|eschtml}</span></td>
<td align="center"><textarea id="input-src-tr-{$record['infos'][0]}" readonly="readonly">{$record['infos'][1]|eschtml}</textarea></td>
<td align="center" width="24px" class="paste-area">
	{if $record['infos'][4]}
		<img src="/images/image.png" />
		<div><img src="/images/translations/{$id}__{$record['infos'][0]}.png" /></div>
	{else}
		{allow translator:upload}
		<input id="img-{$record['infos'][0]}"/>
		{/allow}
	{/if}
</td>
<td align="center" width="24px"><a href="#" title="deepl"><i class="fas fa-language"></i></a></td>
<td align="center"><textarea id="input-tr-{$record['infos'][0]}" tabindex="1" onfocus="displayTranslation{$type}Options(this, '{$src_lang}', '{$dst_lang}', '{$header}');" name="value[{$record['infos'][0]}]" class="round">{$record['infos'][2]|eschtml}</textarea></td>

{if $record['infos'][2] == $record['infos'][3]} 
	<td align="center" width="24px"><i id="check-{$record['infos'][0]}" class="fas fa-check-square padding-check"></i></a></td>
{else}
	<td align="center" width="24px" ><a href="#"  onclick="return ajaxCheck('{$record['infos'][0]}', '{ryurl '', array('id' => $id, 'value' => $record['infos'][0], $offsetParameterName=>$page)}')" title="validate"><i id="check-{$record['infos'][0]}" class="far fa-square"></i></a></td>
{/if}

<td align="center">
	<a title="Revert to Atys version" href="{ryurl '', array('id' => $id, 'value'=>$record['id'], 'command' => 'revert', $offsetParameterName=>$page)}"><i class="fa-solid fa-backward"></i></a>
	{if $can_delete}
		&nbsp;&nbsp;&nbsp;<a href="{ryurl '', array('id' => $id, 'value'=>$record['id'], 'command' => 'delete', $offsetParameterName=>$page)}"><i class="fas fa-trash-alt"></i></a>
	{/if}
</td>
