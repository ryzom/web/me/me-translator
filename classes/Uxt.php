<?php

namespace Tr;

class Uxt extends Core {
	
	private function getFilename($lang) {
		$filename = \jApp::wwwPath().'translations/'.self::$project.$lang.'.uxt';
		return $filename;
	}

	private function readFile($filename) {
		$translations = [];
		if (!file_exists($filename))
			file_put_contents($filename, '');
		$lines = explode("\n", file_get_contents($filename));
		$current_id = '';
		$current_text = '';
		foreach($lines as $line) {
			$sline = explode("\t", $line);
			if ($line && $line[0] != '/' && count($sline) > 1) {
				$id = $sline[0];
				$text = $sline[1];
				
				if ($id)
					$current_id = $id;
				$current_text .= $text;
			} else {
				if ($current_id)
					$translations[$current_id] = str_replace('\n', "\n", substr($current_text, 1, strlen($current_text)-2));
				$current_id = '';
				$current_text = '';
			}
		}
		if ($current_id)
			$translations[$current_id] =  str_replace('\n', "\n", $current_text);
		return $translations;
	}

	private function saveFile($translations, $filename) {
		$out = '';
		if ($translations) {
			foreach($translations as $translation => $text) {
				$text = str_replace("\n", '\n', $text);
				$stext = explode('\n', $text);
				if (count($stext) == 1) {
					$out .= $translation."\t".'['.$text.']';
				} else {
					$text0 = array_shift($stext);
					$out .= $translation."\t".'['.$text0.'\n'."\n\t";
					$out .= implode('\n'."\n\t", $stext).']';
				}
				$out .= "\n\n";
			}
			file_put_contents($filename, str_replace("\r", '', $out));
		}
	}

	protected function loadTranslations($lang='') {
		self::$translations = [];
		$langs = \Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			$translations = $this->readFile($this->getFilename($lang));
			self::$translations[$lang] = $translations;
			if ($lang != 'wk') {
				$translations = $this->readFile($this->getFilename($lang.'_wk'));
				self::$translations[$lang.'_wk'] = $translations;
			}
			ksort(self::$translations[$lang], SORT_STRING|SORT_NATURAL|SORT_FLAG_CASE);
			$this->updateStats($lang);
		}
	}
	
	
	
	private function saveTranslations($lang='') {
		$langs = \Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			ksort(self::$translations[$lang], SORT_STRING|SORT_NATURAL| SORT_FLAG_CASE);
			$translations = self::$translations[$lang];
			$this->saveFile($translations, $this->getFilename($code));
			if ($lang != 'wk') {
				$translations = self::$translations[$lang.'_wk'];
				$this->saveFile($translations, $this->getFilename($lang.'_wk'));
			}
		}
	}
	
	public function countAll() {
		return count(self::$translations['wk']);
	}

	public function countTranslated($lang) {
		$lang = $this->getLang($lang);
		if ($lang == 'wk')
			$translated = self::$translations['wk'];
		else
			$translated = self::$translations[$lang.'_wk'];
		
		$nbr_translated = 0;
		foreach($translated as $name => $translation)
			$nbr_translated += $translation != '';
		return $nbr_translated;
	}

	public function countValidated($lang) {
		$lang = $this->getLang($lang);
		$validated = self::$translations[$lang];
		$nbr_validated = 0;
		foreach($validated as $name => $translation)
			$nbr_validated += $translation != '';
		return $nbr_validated;
	}
	
	public function updateStats($lang) {
		$translated = $this->countTranslated($lang);
		$validated = $this->countValidated($lang);
		Projects::updateProjectLangStats(self::$name, $lang, $translated, $validated);
	}
	
	public function getTranslations($lang, $offset=0, $size=NULL) {
		if (!$size)
			return self::$translations[$lang];
		$keys = array_slice(array_keys(self::$translations[$lang]), $offset, $size);
		$final = [];
		foreach($keys as $key)
			$final[$key] = self::$translations[$lang][$key];
		return $final;
	}
	
	public function searchTranslations($searchword, $src_lang, $dst_lang) {
		$src_lang = $this->getLang($src_lang);
		$dst_lang = $this->getLang($dst_lang);
		$src_translations = self::$translations[$src_lang];
		if ($dst_lang == 'wk')
			$dst_translations = self::$translations[$dst_lang];
		else
			$dst_translations = self::$translations[$dst_lang.'_wk'];
		
		$matches = array();
		foreach($src_translations as $k => $v) {
			if (stripos($k, $searchword) !== false || stripos($v, $searchword) !== false) {
				$matches[$k] = $v;
			}
		}
		
		foreach($dst_translations as $k => $v) {
			if (stripos($v, $searchword) !== false)
				$matches[$k] = $v;
		}
		return $matches;
	}

	public function getLatestAdded() {
	}
	
	public function setTranslations($code, $translations) {
		$lang = $this->getLang($code);

		foreach($translations as $name => $value) {
			if ($lang != 'wk') {
				if (!$value)
					unset(self::$translations[$lang.'_wk'][$name]);
				else
					self::$translations[$lang.'_wk'][$name] = $value;
			} else {
				self::$translations[$lang][$name] = $value;
			}
		}
		if ($lang != 'wk')
			$this->saveFile(self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
		else
			$this->saveFile(self::$translations[$lang], $this->getFilename($lang));
		$this->updateStats($lang);
	}
	
	public function validateTranslations($dst_lang, $value) {
		$lang = $this->getLang($dst_lang);
		
		self::$translations[$lang][$value] = self::$translations[$lang.'_wk'][$value];
		$this->saveFile(self::$translations[$lang], $this->getFilename($lang));
		$this->updateStats($lang);
	}
	
	public function unvalidateTranslations($dst_lang, $value) {
	}
	
	public function addSentence($name) {
		$langs = \Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			if (!isset(self::$translations[$lang][$name])) {
				self::$translations[$lang][$name] = '';
				$this->saveFile(self::$translations[$lang], $this->getFilename($lang));
				if ($lang != 'wk') {
					self::$translations[$lang.'_wk'][$name] = '';
					$this->saveFile(self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
				}
				$this->updateStats($lang);
			}
		}
	}
	
	public function delSentence($name) {
		$langs = \Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			unset(self::$translations[$lang][$name]);
			$this->saveFile(self::$translations[$lang], $this->getFilename($lang));
			if ($lang != 'wk') {
				unset(self::$translations[$lang.'_wk'][$name]);
				$this->saveFile(self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
			}
			$this->updateStats($lang);
		}
	}
	
	public function getHeader($lang) {
		return '';
	}
}