<?php

namespace Tr;
use Ry\DB;
use Ry\Common;

class Phrase extends Core {
	
	public function loadTranslations($lang='') {
	}
	
	public function saveTranslations($lang='') {
		
	}
	
	public function countAll() {
		
	}

	public function countTranslated($lang) {
		
	}

	public function countValidated($lang) {
		
	}
	
	public function getLatestAdded() {
	}


	public function updateStats($lang) {
	}
	
	public function getTranslations($lang, $offset=0, $size=NULL) {
		return ['THIS FILE CAN\'T BE EDITED'];
	}
	
		
	private function _searchInFile($file, $text) {
		$handle = fopen($file, 'r');
		$valid = [];
		while (($buffer = fgets($handle)) !== false) {
			if (stripos($buffer, $text) !== false) {
				$valid[] = $buffer;
				break;
			}
		}
		fclose($handle);
		return $valid;
	}
	
	public function searchTranslations($searchword, $src_lang, $dst_lang) {
		$filename = \jApp::wwwPath().'translations/'.self::$project.$src_lang.'.txt';
		return $this->_searchInFile($filename, $searchword);
	}
	
	public function setTranslations($lang, $translations) {
		
	}
	
	public function validateTranslations($dst_lang, $value) {
		
	}
	
	public function unvalidateTranslations($dst_lang, $value) {

	}
	
	public function addSentence($name) {
	
	}
	
	public function delSentence($name) {
		
	}
		
	public function getHeader($lang) {
		return '';
	}
}