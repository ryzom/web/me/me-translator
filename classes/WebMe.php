<?php

namespace Tr;

use \Ry;
use \Jelix\PropertiesFile\Properties;
use \Jelix\PropertiesFile\Parser;
use \Jelix\PropertiesFile\Writer;


class WebMe extends Core {

	private function getProjectAndFilename() {
		$sproject = explode('~', self::$project);
		if (count($sproject) > 1) {
			$project = $sproject[0];
			$filename = $sproject[1];
		} else {
			$project = self::$project;
			$filename = $project;
		}
		return [$project, $filename];
	}

	private function getPropertiesPath($code) {
		list($project, $filename) = $this->getProjectAndFilename();
		return \jApp::appPath().'modules/'.$project.'/locales/'.$code.'/';
	}
	
	private function getPropertiesFilename($code) {
		$path = $this->getPropertiesPath($code);
		list($project, $filename) = $this->getProjectAndFilename();
		$filename = $path.$filename.'.UTF-8.properties';
		if (!file_exists($filename)) {
			if (!is_dir($path))
				mkdir($path, 0777, true);
			$translations = new Properties();
			$writer = new Writer();
			$writer->writeToFile($translations, $filename); 
		}
		return $filename;
	}
	
	private function getBackupFilename($code) {
		list($project, $filename) = $this->getProjectAndFilename();
		$path = $this->getPropertiesPath($code).'history/';
		$filename = $path.$filename.'.log';
		if (!file_exists($filename)) {
			if (!is_dir($path))
				mkdir($path, 0777, true);
		}
		return $filename;
	}
	
	
	public function getCode($lang) {
		if (strlen($lang) == 2) {
			$langs = Ry\Common::getLangs();
			$lang = $langs[$lang];
		}
		return substr($lang, 0, 3).strtoupper(substr($lang, 3));
	}	

	public function loadTranslations($lang='') {
		self::$translations = [];
		$reader = new Parser();
		$langs = Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			$translations = new Properties();
			$reader->parseFromFile($this->getPropertiesFilename($code), $translations);
			self::$translations[$code] = $translations;
			if ($lang != 'wk') {
				$translations_wk = new Properties();
				$reader->parseFromFile($this->getPropertiesFilename($lang.'_WK'), $translations_wk);
				self::$translations[$lang.'_WK'] = $translations_wk;
			}
		}
	}
	
	public function saveTranslations($lang='') { // UNUSED???
		$writer = new Writer();
		$langs = Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			$translations = self::$translations[$lang];
			$writer->writeToFile($translations , $this->getPropertiesFilename($code));
			if ($lang != 'wk') {
				$translations = self::$translations[$lang.'_WK'];
				$writer->writeToFile($translations , $this->getPropertiesFilename($lang.'_WK'));
			}
		}
	}
	
	public function countAll() {
		$translations = self::$translations['wk_WK']->getAllProperties();
		return count($translations);
	}

	public function countTranslated($lang) {
		$lang = $this->getLang($lang);
		$translated = self::$translations[$lang.'_WK']->getAllProperties();
		$wk = self::$translations['wk_WK']->getAllProperties();
		$nbr_translated = 0;
		foreach($translated as $name => $translation) {
			$nbr_translated += $translation != '';
		}
		return $nbr_translated;
	}

	public function countValidated($lang) {
		$code = $this->getCode($lang);
		$validated = self::$translations[$code]->getAllProperties();
		$nbr_validated = 0;
		foreach($validated as $name => $translation)
			$nbr_validated += $translation != '';
		return $nbr_validated;
	}
	
	public function updateStats($lang) {
		$translated = $this->countTranslated($lang);
		$validated = $this->countValidated($lang);
		Projects::updateProjectLangStats(self::$name, $lang, $translated, $validated);
	}
	
	public function getTranslations($lang, $offset=0, $size=NULL, $keys=[]) {
		$code = $this->getCode($lang);
		$translations = self::$translations[$code]->getAllProperties();
		if (!$keys)
			ksort($translations, SORT_STRING|SORT_NATURAL|SORT_FLAG_CASE);
		if (!$keys && !$size)
			return $translations;
		if (!$keys)
			$keys = array_slice(array_keys($translations), $offset, $size);
		$final = [];
		foreach($keys as $key) {
			if (isset($translations[$key]))
				$final[$key] = $translations[$key];
		}
		return $final;
	}
	
	public function searchTranslations($searchword, $src_lang, $dst_lang) {
		$src_lang = $this->getCode($src_lang);
		$dst_lang = $this->getLang($dst_lang);
		$src_translations = self::$translations[$src_lang]->getAllProperties();
		$dst_translations = self::$translations[$dst_lang.'_WK']->getAllProperties();
		
		$matches = array();
		foreach($src_translations as $k => $v) {
			if (stripos($k, $searchword) !== false || stripos($v, $searchword) !== false) {
				$matches[$k] = $v;
			}
		}
		
		foreach($dst_translations as $k => $v) {
			if (stripos($v, $searchword) !== false)
				$matches[$k] = $v;
		}
		return $matches;
	}
	
	public function getLatestAdded() {
		return $this->getBackupLatestAdded($this->getBackupFilename('wk_WK'));
	}
	
	public function setTranslations($lang, $translations) {
		$lang = $this->getLang($lang);
		$translated = self::$translations[$lang.'_WK'];
		$changes = [];
		$all_translations = $translated->getAllProperties();
		$locker = new Ry\FileLocker('translations_'.self::$project.$lang.'_WK.lock');
		if ($locker->locked()) {
			foreach($translations as $name => $value) {
				if ($value && $all_translations[$name] != $value) {
					$translated->set($name, $value);
					$changes[] = [$name, $all_translations[$name], $value];
				}
			}
			$writer = new Writer();
			$writer->writeToFile($translated , $this->getPropertiesFilename($lang.'_WK'));
			$this->backupChanges($this->getBackupFilename($lang.'_WK'), '=', $changes);
			$this->updateStats($lang);
			$this->updateStats('wk');
			$locker->close();
		}
	}
	
	public function validateTranslations($dst_lang, $value) {
		$lang = $this->getLang($dst_lang);
		$code = $this->getCode($dst_lang);
		$translated_wk = self::$translations[$lang.'_WK'];
		$translated = self::$translations[$code];

		$locker = new Ry\FileLocker('translations_'.self::$project.$lang.'.lock');
		if ($locker->locked()) {
			$translated->set($value, $translated_wk->get($value));
			$writer = new Writer();
			$writer->writeToFile($translated , $this->getPropertiesFilename($code));
			$this->backupChanges($this->getBackupFilename($code), '=', [$value => $translated_wk->get($value)]);
			$locker->close();
			$this->updateStats($dst_lang);
		}
	}
	
	public function unvalidateTranslations($dst_lang, $value) {
		// TODO : unvalidate = undo
		
		return;
		/*
		if (strlen($dst_lang) > 2) {
			$code = $dst_lang;
		} else {
			$langs = Ry\Common::getLangs();
			$code = $langs[$dst_lang];
		}
		
		$translated = self::$translations[$code];
		$locker = new Ry\FileLocker('translations_'.self::$project.'.lock');
		if ($locker->locked()) {
			$translated->set($value, '');
			$writer = new Writer();
			$writer->writeToFile($translated , $this->getPropertiesFilename($code));
			$locker->close();
			$this->updateStats($dst_lang);
		}*/
	}
	
	public function addSentence($name) {
		$writer = new Writer();
		$langs = Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			$translations = self::$translations[$code];
			$locker = new Ry\FileLocker('translations_'.self::$project.$lang.'_WK.lock');
			if ($locker->locked()) {
				if ($lang == 'wk')
					$translations->set($name, '_'.ucfirst($name));
				else
					$translations->set($name, '');
				$translated = $translations->getAllProperties();
				$writer->writeToFile($translations, $this->getPropertiesFilename($code));
				$this->backupChanges($this->getBackupFilename($code), '+', $name);
				if ($lang != 'wk') {
					$translations_wk = self::$translations[$lang.'_WK'];
					$translations_wk->set($name, '');
					$translated = $translations_wk->getAllProperties();
					$writer->writeToFile($translations_wk , $this->getPropertiesFilename($lang.'_WK'));
					$this->backupChanges($this->getBackupFilename($lang.'_WK'), '+', $name);
				}
				$locker->close();
				$this->updateStats($lang);
				$this->updateStats($lang.'_WK');
			}
		}
	}
	
	public function delSentence($name) {
		$writer = new Writer();
		$langs = Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			$translations = self::$translations[$code];
			$locker = new Ry\FileLocker('translations_'.self::$project.$lang.'_WK.lock');
			if ($locker->locked()) {
				$translations->offsetUnset($name);
				$writer->writeToFile($translations , $this->getPropertiesFilename($code));
				$this->backupChanges($this->getBackupFilename($code), '-', $name);
				if ($lang != 'wk') {
					$translations = self::$translations[$lang.'_WK'];
					$translations->offsetUnset($name, '');
					$writer->writeToFile($translations , $this->getPropertiesFilename($lang.'_WK'));
					$this->backupChanges($this->getBackupFilename($lang.'_WK'), '-', $name);
				}
				$locker->close();
				$this->updateStats($lang);
				$this->updateStats($lang.'_WK');
			}
		}
	}
	
	public function getHeader($lang) {
		return '';
	}
}