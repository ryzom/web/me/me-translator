<?php

namespace Tr;

class Words extends Core {
		
	private function getFilename($lang) {
		if (self::$project == 'bot_names')
			$filename = \jApp::wwwPath().'translations/bot_names.txt';
		else
			$filename = \jApp::wwwPath().'translations/'.self::$project.'_words_'.$lang.'.txt';
		return $filename;
	}


	private function readFile($lang, $filename) {
		$translations = [];
		$lines = explode("\n", file_get_contents($filename));
		self::$headers[$lang] = array_shift($lines);
		foreach($lines as $line) {
			$sline = explode("\t", str_replace('\d', '', $line));
			if ($line && $line[0] != '*' && count($sline) > 2 && substr($sline[2], 0, 5) != '<GEN>') {
				$hash = intval(array_shift($sline));
				$id = array_shift($sline);
				$translations[$id] = [$hash, implode("\n", $sline), $id];
			}
		}
		return $translations;
	}

	private function saveFile($lang, $translations, $filename) {
		$lines = explode("\n", file_get_contents($filename));
		$out = $lines[0]."\n";
		foreach($translations as $translation => $text) {
			$out .= ($text[0] == 0?'-':$text[0])."\t".$translation."\t".str_replace("\n", "\t", $text[1])."\n";
		}
		file_put_contents($filename,  str_replace('⇶ ', '', str_replace("\r", '', $out)));
	}

	protected function loadTranslations($lang='') {
		self::$translations = [];
		self::$sorted_translations = [];

		$langs = \Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			$translations = $this->readFile($lang, $this->getFilename($lang));
			self::$translations[$lang] = $translations;
			self::$sorted_translations[$lang] = false;
			//uasort(self::$translations[$lang], [$this, 'cmp']);
			if ($lang != 'wk') {
				$translations = $this->readFile($lang, $this->getFilename($lang.'_wk'));
				self::$translations[$lang.'_wk'] = $translations;
			}
			self::$sorted_translations[$lang.'_wk'] = false;
			//uasort(self::$translations[$lang.'_wk'], [$this, 'cmp']);
		}
		
		//uasort(self::$translations['wk'], [$this, 'cmp']);
	}

	private function cmp($a, $b)
	{
		if ($a[0] == $b[0])
			return strcasecmp($a[2], $b[2]);
		return $a[0] < $b[0] ? 1 : -1;
	}

	private function saveTranslations($lang='') {
		$langs = \Ry\Common::getLangs();
		if ($lang)
			$langs = [$langs[$lang]];
		foreach($langs as $lang => $code) {
			//ksort(self::$translations[$lang], SORT_STRING|SORT_NATURAL| SORT_FLAG_CASE);
			$translations = self::$translations[$lang];
			$this->saveFile($lang, $translations, $this->getFilename($code));
			if ($lang != 'wk') {
				$translations = self::$translations[$lang.'_wk'];
				$this->saveFile($lang, $translations, $this->getFilename($lang.'_wk'));
			}
		}
	}
	
	public function countAll() {
		return count(self::$translations['wk']);
	}

	public function countTranslated($lang) {
		$lang = $this->getLang($lang);
		if ($lang == 'wk')
			return count(self::$translations['wk']);

		$translated = self::$translations[$lang.'_wk'];
		$nbr_translated = 0;
		foreach($translated as $name => $translation) {
			if (trim($translation[1]) != '')
				$nbr_translated++;
		}
		return $nbr_translated;
	}

	public function countValidated($lang) {
		if ($lang == 'wk')
			return 0;
		$lang = $this->getLang($lang);
		$translated = self::$translations[$lang.'_wk'];
		$validated = self::$translations[$lang];
		$nbr_validated = 0;
		foreach($validated as $name => $translation) {
			if (isset($translated[$name]) && $translated[$name][1] == $translation[1])
				$nbr_validated++;
		}
		return $nbr_validated;
	}

	public function updateStats($lang) {
		$translated = $this->countTranslated($lang);
		$validated = $this->countValidated($lang);
		Projects::updateProjectLangStats(self::$name, $lang, $translated, $validated);
	}
	
	public function getTranslations($lang, $offset=0, $size=NULL) {
		if (self::$sorted_translations[$lang] == false) {
			uasort(self::$translations[$lang], [$this, 'cmp']);
			self::$sorted_translations[$lang] = true;
		}
		if (!$size)
			$keys = array_keys(self::$translations[$lang]);
		else
			$keys = array_slice(array_keys(self::$translations[$lang]), $offset, $size);
		$final = [];
		foreach($keys as $key)
			$final[$key] = self::$translations[$lang][$key][1];
		return $final;
	}
	
	public function searchTranslations($searchword, $src_lang, $dst_lang) {
		$src_lang = $this->getLang($src_lang);
		$dst_lang = $this->getLang($dst_lang);
		$src_translations = self::$translations[$src_lang];
		if ($dst_lang == 'wk')
			$dst_translations = self::$translations[$dst_lang];
		else
			$dst_translations = self::$translations[$dst_lang.'_wk'];
		
		$matches = array();
		foreach($src_translations as $k => $v) {
			if (stripos($k, $searchword) !== false || stripos($v[1], $searchword) !== false) {
				$matches[$k] = $v[1];
			}
		}
		
		foreach($dst_translations as $k => $v) {
			if (stripos($v[1], $searchword) !== false)
				$matches[$k] = $v[1];
		}
		return $matches;
	}
	
	public function setTranslations($lang, $translations) {
		$wk_lang = $lang = $this->getLang($lang);
		if ($lang != 'wk')
			$wk_lang = $lang.'_wk';
		foreach($translations as $name => $value)
			self::$translations[$wk_lang][$name][1] = $value;
		
		$this->saveFile($lang, self::$translations[$wk_lang], $this->getFilename($wk_lang));
		$this->updateStats($lang);
	}
	
	public function getLatestAdded() {
	}

	public function validateTranslations($dst_lang, $value) {
		$lang = $this->getLang($dst_lang);
		
		self::$translations[$lang][$value][1] = self::$translations[$lang.'_wk'][$value][1];
		$this->saveFile($lang, self::$translations[$lang], $this->getFilename($lang));
		$this->updateStats($lang);
	}
	
	public function unvalidateTranslations($dst_lang, $value) {
	}
	
	public function revertTranslations($dst_lang, $value) {
		$lang = $this->getLang($dst_lang);
		self::$translations[$lang.'_wk'][$value][1] = self::$translations[$lang][$value][1];
		$this->saveFile($lang, self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
		$this->updateStats($lang);
	}
	
	public function addSentence($name) {
		$langs = \Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			if (!isset(self::$translations[$lang][$name])) {
				self::$translations[$lang][$name] = [time(), ''];
				$this->saveFile($lang, self::$translations[$lang], $this->getFilename($lang));
				if ($lang != 'wk') {
					self::$translations[$lang.'_wk'][$name] = [time(), ''];
					$this->saveFile($lang, self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
				}
				$this->updateStats($lang);
			}
		}
	}
	
	public function delSentence($name) {
		$langs = \Ry\Common::getLangs();
		foreach($langs as $lang => $code) {
			unset(self::$translations[$lang][$name]);
			$this->saveFile($lang, self::$translations[$lang], $this->getFilename($lang));
			if ($lang != 'wk') {
				unset(self::$translations[$lang.'_wk'][$name]);
				$this->saveFile($lang, self::$translations[$lang.'_wk'], $this->getFilename($lang.'_wk'));
			}
			$this->updateStats($lang);
		}
	}
	
	public function getHeader($lang) {
		return self::$headers[$lang];
	}
}