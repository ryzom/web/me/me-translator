<?php

namespace Tr;
use Ry;

class Projects {
	
	public static function getList($type='') {
		$db = Ry\DB::spawn('translations', 'translator');
		if (!$type)
			return $db->get('projects', ['#ORDER BY' => [['type', 'DESC'], ['icon', 'DESC'], ['name', 'ASC']]]);
		if ($type == 'Client')
			return $db->get('projects', ['type' => ['IN', ['Uxt', 'Words', 'Phrase']]]);

		return $db->get('projects', ['type' => $type]);
	}
	
	public static function spawn($id) {
		$db = Ry\DB::spawn('translations', 'translator');
		$project = $db->getById('projects', $id);
		p($project, $id);
		switch ($project->type) {
			case 'Ark':
				$instance = Ark::spawn($project->name, $project->file);
				break;
			
			case 'Me':
				$instance = WebMe::spawn($project->name, $project->file);
				break;
			
			case 'Uxt':
				$instance = Uxt::spawn($project->name, $project->file);
				break;
			
			case 'Words':
				$instance = Words::spawn($project->name, $project->file);
				break;

			case 'Phrase':
				$instance = Phrase::spawn($project->name, $project->file);
				break;
		}
		return $instance;
	}
	
	public static function updateProjectLangStats($name, $lang, $translated, $validated='') {
		$db = Ry\DB::spawn('translations', 'translator');
		if ($lang == 'wk')
			$db->update('projects', ['total' => $translated], ['name' => $name]);
		else
			$db->update('projects', [$lang => $validated.'/'.$translated], ['name' => $name]);
		p($translated.'/'.$validated, $lang);
	}
}