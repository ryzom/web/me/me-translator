<?php

namespace Tr;
use Ry\DB;
use Ry\Common;
use Ry\Account;

class Ark extends Core {
	protected $all_langs = array('wk' => 0, 'en' => 1, 'de' => 2, 'es' => 3, 'fr' => 4, 'ru' => 5);
	
	public function loadTranslations($lang='') {
		$db = DB::spawn('ark', 'translator');
		$event = $db->getSingle('events', ['id' => self::$project]);
		$db = DB::spawn('translations', 'translator');
		$wk = $db->get('ark_temp', ['event_id' => self::$project]);
		//p($wk, 'ark_temp');
		self::$translations = [];
		$langs_ark_order = ['wk', 'en', 'fr', 'ru', 'es', 'de'];
		if ($event && $event->trads) {
			$translations = unserialize($event->trads);
			$langs = Common::getLangs();
			foreach($langs as $lang => $code) {
				self::$translations[$lang] = [];
				if ($lang != 'wk')
					self::$translations[$lang.'_wk'] = [];
			}

			foreach($translations as $id => $values) {
				$i = 0;
				foreach($langs_ark_order as $lang) { // Set wk_XX for Ark events who don't have it.
					$code = $langs[$lang];
					if (!isset($values[$lang])) {
						if ($lang == 'wk' && $values['en'])
							$values[$lang] = $values['en'];
						elseif ($lang == 'wk' && $values['fr'])
							$values[$lang] = $values['fr'];
						else 
							$values[$lang] = '';
					}
					if ($lang == 'fr')
						p($id, $values[$lang]);
					self::$translations[$lang][$id] = $values[$lang];
					
					if ($lang != 'wk') {
						$db_id = $id.'|'.self::$project;
						if (isset($wk[$db_id]) && $wk[$db_id] && $wk[$db_id]->$lang && strtotime($wk[$db_id]->date) > $values['timestamp'][$i])
							self::$translations[$lang.'_wk'][$id] = $wk[$db_id]->$lang;
						else
							self::$translations[$lang.'_wk'][$id] = $values[$lang];
					}
					$i++;
				}
			}
		}
	}
	
	public function saveTranslations($lang='') {
		
	}
	
	public function countAll() {
		
	}

	public function countTranslated($lang) {
		$db = \Ry\DB::spawn('translations', 'translator');
		$translated = 0;
		$trads = self::$translations[$lang];
		$wk_trads = self::$translations['wk'];
		if ($lang == 'wk')
			return count($wk_trads);

		foreach($trads as $id => $trad) {
			$wk = $db->getSingle('ark_temp', ['id' => $id, 'event_id' => self::$project]);
			if (
				($trad || substr($wk_trads[$id], 0, 8) == '[notrad]') || substr($wk_trads[$id], 0, 3) == '[e]' ||
				($wk && ($wk->$lang || substr($wk->wk, 0, 8) == '[notrad]' || substr($wk->wk, 0, 3) == '[e]')) 
				)
				$translated++;
		}
		return $translated;
	}

	public function countValidated($lang) {
		$db = \Ry\DB::spawn('translations', 'translator');
		$validated = 0;
		$trads = self::$translations[$lang];
		$wk_trads = self::$translations['wk'];
		foreach($trads as $id => $trad) {
			$wk = $db->getSingle('ark_temp', ['id' => $id, 'event_id' => self::$project]);
			if (($trad && (!$wk || !isset($wk->$lang) || $wk->$lang == $trad)) || substr($wk_trads[$id], 0, 8) == '[notrad]' || substr($wk_trads[$id], 0, 3) == '[e]')
				$validated++;
		}
		return $validated;
	}
	
	public function getLatestAdded() {
		$ret = array_keys(self::$translations['wk']);
		$ret = array_reverse($ret);
		return $ret;
	}


	public function updateStats($lang) {
		$translated = $this->countTranslated($lang);
		$validated = $this->countValidated($lang);
		Projects::updateProjectLangStats(self::$name, $lang, $translated, $validated);
		$translated = $this->countTranslated('wk');
		Projects::updateProjectLangStats(self::$name, 'wk', $translated);
	}
	
	public function getTranslations($code, $offset=0, $size=NULL, $keys=[]) {
		
		$translations = self::$translations[$code];
		if (!$keys)
			ksort($translations, SORT_STRING|SORT_NATURAL|SORT_FLAG_CASE);
		if (!$keys && !$size)
			return $translations;
		if (!$keys)
			$keys = array_slice(array_keys($translations), $offset, $size);
		$final = [];
		foreach($keys as $key) {
			if (isset($translations[$key]))
				$final[$key] = $translations[$key];
		}
		return $final;
	}
	
	public function searchTranslations($searchword, $src_lang, $dst_lang) {
		$src_lang = $this->getLang($src_lang);
		$dst_lang = $this->getLang($dst_lang);
		$src_translations = self::$translations[$src_lang];
		if ($dst_lang == 'wk')
			$dst_translations = self::$translations[$dst_lang];
		else
			$dst_translations = self::$translations[$dst_lang.'_wk'];
		
		$matches = array();
		foreach($src_translations as $k => $v) {
			if (stripos($k, $searchword) !== false || stripos($v, $searchword) !== false) {
				$matches[$k] = $v;
			}
		}
		
		foreach($dst_translations as $k => $v) {
			if (stripos($v, $searchword) !== false)
				$matches[$k] = $v;
		}
		return $matches;
	}
	
	public function setTranslations($code, $translations) {
		$db = DB::spawn('translations', 'translator');
		$lang = $this->getLang($code);
		$code = 'wk';
		if ($lang != 'wk')
			$code = $lang.'_wk';
		
		foreach($translations as $name => $value) {
			if ($code != 'wk') {
				if (!isset(self::$translations[$code][$name]) || self::$translations[$code][$name] != $value) {
					self::$translations[$code][$name] = $value;
					$db->insertOrUpdate('ark_temp', [$lang => $value, 'date' => DB::now()], ['event_id' => self::$project, 'id' => $name]);
				}
			} else {
				$db->update('ark_temp', ['wk' => $value, 'date' => DB::now()], ['event_id' => self::$project, 'id' => $name]);
				if (!isset(self::$translations['wk'][$name]) || self::$translations['wk'][$name] != $value) {
					self::$translations['wk'][$name] = $value;
					$this->validateTranslations('wk', $name, $value);
				}
			}
		}
		$this->updateStats($lang);
	}
	
	public function validateTranslations($dst_lang, $name, $value='') {
		if (!$value) {
			$db = DB::spawn('translations', 'translator');
			$wk = $db->getSingle('ark_temp', ['event_id' => self::$project, 'id' => $name]);
			$value = $wk->$dst_lang;
		}
 
		$db = DB::spawn('ark', 'translator'); 
		$event = $db->getSingle('events', ['id' => self::$project]);
		if ($event && $event->trads) {  
			$translations = unserialize($event->trads);
			if (!isset($translations[$name]))
				$translations[$name] = [];
			$translations[$name][$dst_lang] = $value; 
			if (!$translations[$name]['timestamp'])
				$translations[$name]['timestamp'] = [0, 0, 0, 0, 0, 0, 0];
			$translations[$name]['timestamp'][$this->all_langs[$dst_lang]] = time();
			$user = Account::spawn();
			if (!$translations[$name]['user']) 
				$translations[$name]['user'] = ['', '', '', '', '', ''];
			$translations[$name]['user'][$this->all_langs[$dst_lang]] = $user->name;
			$db->update('events', ['trads' => serialize($translations)], ['id' => self::$project]);
		}
		$this->updateStats($dst_lang);
	}
	
	public function unvalidateTranslations($dst_lang, $value) {
		// Nothing to do
	}
	
	public function revertTranslations($dst_lang, $value) {
		
	}
	
	
	public function addSentence($name) {
		$db = DB::spawn('translations', 'translator');
		if (!$db->getSingle('ark_temp', ['id' => $name])) {
			$db->insert('ark_temp', ['event_id' => self::$project, 'id' => $name, 'date' => DB::now()]);
			$this->validateTranslations('wk', $name, '');
			$this->updateStats($dst_lang); 
		}
	}
	
	public function delSentence($name) {
		$db = DB::spawn('translations', 'translator');
		$db->delete('ark_temp', ['event_id' => self::$project, 'id' => $name]);
		$db = DB::spawn('ark', 'translator');
		$event = $db->getSingle('events', ['id' => self::$project]);
		$translations = unserialize($event->trads);
		unset($translations[$name]);
		$db->update('events', ['trads' => serialize($translations)], ['id' => self::$project]);
		$this->updateStats($dst_lang);
	}
		
	public function getHeader($lang) {
		return '';
	}
}