<?php

namespace Tr;


abstract class Core {
	
	public static $name = '';
	protected static $_instances = [];
	protected static $project = '';
	protected static $translations = [];
	protected static $sorted_translations = [];
	protected static $headers = [];
	
	function __construct($name, $project) {
		self::$name = $name;
		self::$project = $project;
		$this->loadTranslations();
	}

	
	public static function spawn($name, $project='') {
		if (!isset(self::$_instances[$name]) || self::$_instances[$name] == null) {
			$class = get_called_class();
			self::$_instances[$name] = new $class($name, $project);
		}
		return self::$_instances[$name];
	}

	public function getLang($lang) {
		if (strlen($lang) > 2) {
			$slang = explode('_', $lang);
			$lang = $slang[0];
		}
		return $lang;
	}
	
	public function getCode($lang) {
		if (strlen($lang) == 2) {
			$langs = \Ry\Common::getLangs();
			$lang = $langs[$lang];
		}
		return $lang;
	}
	
	public function getSrcLang() {
		return isset($_SESSION['translator-src-lang'])?$_SESSION['translator-src-lang']:'wk';
	}

	public function getDstLang() {
		return isset($_SESSION['translator-dst-lang'])?$_SESSION['translator-dst-lang']:Ry\Common::getUserLang();
	}

	public function backupChanges($filename, $action, $changes) {
		// Todo : check size and create new file if need
		$file = fopen($filename, 'a+');
		
		if (is_array($changes)) {
			$all_changes = [];
			foreach($changes as $change)
				$all_changes[] = str_replace(["\r", "\n"], ['', '\n'], $change);
			$changes = serialize($all_changes);
		}
		fwrite($file, $action.' '.$changes."\n");
		fclose($file);
	}
	
	public function getBackupLatestAdded($filename) {
		$logs = explode("\n", file_get_contents($filename));
		$latest = [];
		for($i = count($logs)-1; $i >= 0; $i--) {
			if ($logs[$i] && $logs[$i][0] == '+') {
				$latest[] = substr($logs[$i], 2);
			}
		}
		return $latest;
	}

	abstract protected function loadTranslations();
	abstract public function countAll();
	abstract public function countTranslated($lang);
	abstract public function countValidated($lang);
	abstract public function getTranslations($lang, $offset, $size=NULL);
	abstract public function getLatestAdded();
	abstract public function getHeader($lang);
	abstract public function searchTranslations($searchword, $src_lang, $dst_lang);


}
